from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from .views import home

# Create your tests here.

class Story10UnitTest(TestCase):

    def test_page(self):
        response = Client().get("/")
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'myapp/index.html')
    
    def test_header(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hello",html_response)